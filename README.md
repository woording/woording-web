woording-web
============
The Woording SPA, Work in progress.  
Built using VueJS  

NOTE: This isn't the same site as the one currently live on `http://woording.com`. The version that's currently live is a working, yet unmaintanable, older version built using AngularJS. This repository contains the site that is currently being built to replace it, it'll hopefully go live soon.

## How to run
```bash
npm install
npm run dev
```

## How to deploy
```bash
./deploy.sh
# Type in passwords when it prompts you

# After a while 
# (around 5 minutes, depending on the speed of the PC you're deploying from)

# Type in the password of the cor user on the server when it prompts you

# Then you'll eventually see something like running on 8080, then hit Ctrl A D
```



